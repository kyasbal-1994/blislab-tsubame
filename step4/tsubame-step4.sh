#!/bin/sh
#$ -cwd
#$ -l f_node=1
#$ -l h_rt=0:01:00
/etc/profile.d/modules.sh
cd ./test
module load cuda intel intel-mpi
./run_bl_dgemm.sh
